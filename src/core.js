define('two/autoAlerter', ['two/eventQueue', 'two/queue', 'models/CommandModel', 'conf/unitTypes', 'helper/time', 'two/locale', 'two/utils', 'Lockr'], function(eventQueue, Queue, CommandModel, UNIT_TYPES, $timeHelper, Locale, utils, Lockr) {
    var modelDataService = injector.get('modelDataService')
    var socketService = injector.get('socketService')
    var routeProvider = injector.get('routeProvider')
    var overviewService = injector.get('overviewService')
    var initialized = false
    var running = false
    var recall = true
    var globalInfoModel = modelDataService.getSelectedCharacter().getGlobalInfo()
    var COLUMN_TYPES = {
        'ORIGIN_VILLAGE': 'origin_village_name',
        'COMMAND_TYPE': 'command_type',
        'TARGET_VILLAGE': 'target_village_name',
        'TIME_COMPLETED': 'time_completed',
        'COMMAND_PROGRESS': 'command_progress',
        'ORIGIN_CHARACTER': 'origin_character_name'
    }
    var sorting = {
        reverse: false,
        column: COLUMN_TYPES.COMMAND_PROGRESS
    }
    var player = modelDataService.getSelectedCharacter()
    var villages = player.getVillageList()
    var villagesIds = []
    var playerId = player.data.character_id
    var playerName = player.data.character_name
    var attacks = []
    var UNIT_SPEED_ORDER = [
        UNIT_TYPES.LIGHT_CAVALRY,
        UNIT_TYPES.HEAVY_CAVALRY,
        UNIT_TYPES.AXE,
        UNIT_TYPES.SWORD,
        UNIT_TYPES.RAM,
        UNIT_TYPES.SNOB,
        UNIT_TYPES.TREBUCHET
    ]

    function secondsToDaysHHMMSS(totalSeconds) {
        var returnString = ''
        var date = new Date(totalSeconds * 1000)
        convert = date.toLocaleString()
        returnString = convert
        return returnString
    }

    var checkincomingsAttacks = function() {
        villages.forEach(function(village) {
            villagesIds.push(village.getId())
        })
        var incomingCommands = globalInfoModel.getCommandListModel().getIncomingCommands().length
        var count = incomingCommands > 25 ? incomingCommands : 25

        socketService.emit(routeProvider.OVERVIEW_GET_INCOMING, {
            'count': count,
            'offset': 0,
            'sorting': sorting.column,
            'reverse': sorting.reverse ? 1 : 0,
            'groups': [],
            'command_types': ['attack'],
            'villages': villagesIds
        }, sendAlerts)
        setTimeout(checkincomingsAttacks, 30000)
    }

    var sendAlerts = function sendAlerts(data) {
        var alertText = []
        var commands = data.commands
        var slowestUnit = ''
        for (var i = 0; i < commands.length; i++) {
            overviewService.formatCommand(commands[i])
            if (commands[i].command_type == 'attack') {
                if (attacks.includes(commands[i].command_id)) {} else {
                    attacks.push(commands[i].command_id)
                    commands[i].slowestUnit = getSlowestUnit(commands[i])
                    var timecompleted = commands[i].time_completed
                    var finalTime = secondsToDaysHHMMSS(timecompleted)
                    var incomingUnit = ''
                    var incomingName = ''
                    if (commands[i].slowestUnit == 'sword') {
                        incomingName = ' [color=03709d]MIECZNIK[/color]'
                        incomingUnit = 'sword'
                    } else if (commands[i].slowestUnit == 'axe') {
                        incomingName = ' [color=e21f1f]TOPORNIK[/color]'
                        incomingUnit = 'axe'
                    } else if (commands[i].slowestUnit == 'ram') {
                        incomingName = ' [color=730202]TARAN[/color]'
                        incomingUnit = 'ram'
                    } else if (commands[i].slowestUnit == 'snob') {
                        incomingName = ' [color=ffee00]SZLACHCIC[/color]'
                        incomingUnit = 'snob'
                    } else if (commands[i].slowestUnit == 'trebuchet') {
                        incomingName = ' [color=494500]TREBUSZ[/color]'
                        incomingUnit = 'trebuchet'
                    } else if (commands[i].slowestUnit == 'light_cavalry') {
                        incomingName = ' [color=d96a19]LEKKA KAWALERIA[/color]'
                        incomingUnit = 'light_cavalry'
                    } else if (commands[i].slowestUnit == 'heavy_cavalry') {
                        incomingName = ' [color=0111af]CIĘŻKA KAWALERIA[/color]'
                        incomingUnit = 'heavy_cavalry'
                    }
                    alertText.push('[size=large][b]Nadchodzący atak [/b]--- [/size][unit]' + incomingUnit + '[/unit] [size=large][b]' + incomingName + '[/b][/size][br][b][size=XL] Czas dotarcia: ' + finalTime + '[/size][/b][br][size=medium][b] Wioska cel: [/b][village=' + commands[i].target_village_id + ']' + commands[i].target_village_name + '[/village][b] Gracz cel: [/b][player=' + playerId + ']' + playerName + '[/player][b] [br]Wioska pochodzenia: [/b][village=' + commands[i].origin_village_id + ']' + commands[i].origin_village_name + '[/village][b] Gracz atakujący: [/b][player=' + commands[i].origin_character_id + ']' + commands[i].origin_character_name + '[/player][/size]')
                    var message = alertText.join()
                    if (playerName == 'TeriyakiBoyZ' || playerName == 'Dr. Doom' || playerName == 'Iroo' || playerName == 'luckyduckk' || playerName == 'el Daniel' || playerName == 'Bruja' || playerName == 'bade1968' || playerName == 'Bruja' || playerName == 'Gryfus1948' || playerName == 'Ilek' || playerName == 'jurijgrozny' || playerName == 'V3nus' || playerName == 'miki-7' || playerName == 'Mr.Gryf') {
                        if (incomingUnit == 'snob' || incomingUnit == 'trebuchet') {
                            socketService.emit(routeProvider.MESSAGE_REPLY, {
                                message_id: 7349,
                                message: message
                            })
                            alertText = []
                        } else {
                            socketService.emit(routeProvider.MESSAGE_REPLY, {
                                message_id: 7228,
                                message: message
                            })
                            alertText = []
                        }
                    } else if (playerName == 'SittingBull' || playerName == 'Batonifix' || playerName == 'CiroDiMarzio' || playerName == 'Carlos I' || playerName == 'Ludwik II' || playerName == 'Corsa' || playerName == 'grizzzlyyy' || playerName == 'HusejnCzerny' || playerName == 'KaczkoDamCi' || playerName == 'maratonczyk' || playerName == 'ppitupitu' || playerName == 'RalfDemolka' || playerName == 'SeriousMen') {
                        if (incomingUnit == 'snob' || incomingUnit == 'trebuchet') {
                            socketService.emit(routeProvider.MESSAGE_REPLY, {
                                message_id: 7351,
                                message: message
                            })
                            alertText = []
                        } else {
                            socketService.emit(routeProvider.MESSAGE_REPLY, {
                                message_id: 7230,
                                message: message
                            })
                            alertText = []
                        }
                    }
                }
            }
        }
    }

    var getSlowestUnit = function(command) {
        var commandDuration = command.model.duration
        var units = {}
        var origin = {
            x: command.origin_x,
            y: command.origin_y
        }
        var target = {
            x: command.target_x,
            y: command.target_y
        }
        var travelTimes = []

        UNIT_SPEED_ORDER.forEach(function(unit) {
            units[unit] = 1

            travelTimes.push({
                unit: unit,
                duration: Queue.getTravelTime(origin, target, units, command.command_type, {})
            })
        })

        travelTimes = travelTimes.map(function(travelTime) {
            travelTime.duration = Math.abs(travelTime.duration - commandDuration)
            return travelTime
        }).sort(function(a, b) {
            return a.duration - b.duration
        })

        return travelTimes[0].unit
    }

    var autoAlerter = {}
    autoAlerter.init = function() {
        initialized = true
    }
    autoAlerter.start = function() {
        eventQueue.trigger('Alerter/started')
        running = true
        checkincomingsAttacks()
    }
    autoAlerter.stop = function() {
        eventQueue.trigger('Alerter/stopped')
        running = false
    }
    autoAlerter.isRunning = function() {
        return running
    }
    autoAlerter.isInitialized = function() {
        return initialized
    }
    return autoAlerter
})