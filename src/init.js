require([
    'two/ready',
    'two/autoAlerter',
    'Lockr',
    'two/eventQueue',
    'two/autoAlerter/ui',
], function (
    ready,
    autoAlerter,
    Lockr,
    eventQueue
) {
    if (autoAlerter.isInitialized()) {
        return false
    }

    ready(function () {
        autoAlerter.init()
        autoAlerter.interface()
        
        ready(function () {
            if (Lockr.get('alerter-active', false, true)) {
                autoAlerter.start()
            }

            eventQueue.bind('Alerter/started', function () {
                Lockr.set('alerter-active', true)
            })

            eventQueue.bind('Alerter/stopped', function () {
                Lockr.set('alerter-active', false)
            })
        }, ['initial_village'])
    })
})
