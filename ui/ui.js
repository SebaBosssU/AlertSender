define('two/autoAlerter/ui', [
    'two/autoAlerter',
    'two/FrontButton',
    'two/locale',
    'two/utils',
    'two/eventQueue'
], function (
    autoAlerter,
    FrontButton,
    Locale,
    utils,
    eventQueue
) {
    var opener

    function AlerterInterface () {
        Locale.create('alerter', __alerter_locale, 'pl')
        
        opener = new FrontButton(Locale('alerter', 'title'), {
            classHover: false,
            classBlur: false,
            tooltip: Locale('alerter', 'description')
        })

        opener.click(function () {
            if (autoAlerter.isRunning()) {
                autoAlerter.stop()
                utils.emitNotif('success', Locale('alerter', 'deactivated'))
            } else {
                autoAlerter.start()
                utils.emitNotif('success', Locale('alerter', 'activated'))
            }
        })

        eventQueue.bind('Alerter/started', function () {
            opener.$elem.removeClass('btn-green').addClass('btn-red')
        })

        eventQueue.bind('Alerter/stopped', function () {
            opener.$elem.removeClass('btn-red').addClass('btn-green')
        })

        if (autoAlerter.isRunning()) {
            eventQueue.trigger('Alerter/started')
        }

        return opener
    }

    autoAlerter.interface = function () {
        autoAlerter.interface = AlerterInterface()
    }
})
